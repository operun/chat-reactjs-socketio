var userController = (function () {
  var history = [];
  var users = [];

  var getHistory = function () {
    return history;
  };

  var setHistory = function (message) {
    history.push(message)
  };

  var getUsers = function () {
    return users;
  };

  var addUser = function (user) {
    users.push(user);
  };

  var removeUser = function (user) {
    var index = users.indexOf(user)
    if(index >= 0){
      users.splice(index, 1);
    }
  };

  return {
    setHistory: setHistory,
    getHistory: getHistory,
    getUsers: getUsers,
    addUser: addUser,
    removeUser: removeUser
  };
}());

module.exports = function (socket) {
  var data = {};

  data = {
    history: userController.getHistory(),
    users: userController.getUsers()
  }

  socket.emit('init', data);

  socket.join('general', function(){
    // console.log('socket.rooms: '+socket.rooms); // [ <socket.id>, 'general' ]
    // console.log('socket.rooms[socket.id] : '+socket.rooms[socket.id]); // [ <socket.id>, 'general' ]
    // console.log('socket.id: '+socket.id);
    userController.addUser(socket.id);
  });

  socket.broadcast.emit('user:join', socket.id);

  socket.on('user:history', function () {
    if(history.length > 0){
      socket.broadcast.emit('user:history', history);
    }
  });

  socket.on('send:message', function (message) {
    socket.emit('send:message', message);//только себе
    socket.broadcast.emit('send:message', message);//вссем, кроме себя

    userController.setHistory(message)
  });

  socket.on('disconnect', function () {
    console.log('disconnect: '+socket.id);
    socket.broadcast.emit('user:left', socket.id);
    userController.removeUser(socket.id);
  });

};
