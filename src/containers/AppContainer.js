import React, {Component} from 'react'
import {render} from 'react-dom'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Chat from '../components/Chat'
import './styles.scss'

class AppContainer extends Component {
  render () {
    return (
      <div>
        <Header />
        <Chat />
        <Footer />
      </div>
    )
  }
}

export default AppContainer