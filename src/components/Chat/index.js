import React, { Component } from "react";
import './Chat.scss'
import io from 'socket.io-client'

const socket = io.connect('http://localhost:3000')

export default class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      message: '',
      messages: [],
      // discussions:[
      //   {id: 1},
      //   {id: 2},
      //   {id: 3}
      // ],
      discussions:[],
      activeIndex: 0
    };
  }

  componentDidMount() {
    socket.on('connect', ()=>{
      // console.log('socket connected, socket.id: '+socket.id)
    });
    
    socket.on('init', (data)=>{
      if(data.history.length > 0){
        this.setState({messages: data.history})
      }
      console.log('data.users: '+JSON.stringify(data.users));
      if(data.users.length > 0){
        this.setState({discussions: data.users})
      }
      // if(data.history.length > 0){
      //   this.setState({messages: history})
      // }
      // this.setState({users: data.discussions})

    })

    socket.on('user:join', (data)=>{
      console.log('user:join: '+data);
      this.setState((state)=>{
        state.discussions = state.discussions.concat(data)
      })
    })

    socket.on('user:left', (user)=>{
      console.log('user:left: '+user);
      let discussions = this.state.discussions
      let index = discussions.indexOf(user)
      if(index >= 0){
        discussions.splice(index, 1);
      }
      this.setState({discussions: discussions})

    })

    socket.on('send:message', (message)=>{
      console.log("Client recive send:message - "+JSON.stringify(message))

      let messages = this.state.messages
      messages.push(message)
      this.setState({messages: messages})
    })

  }

  handleOnKeyPress(e){
    if(e.key == 'Enter'){
      // this.onAddMessage()
    }
  }

  onAddMessage(){
    if(this.state.message == ''){
      return
    }
    // console.log(JSON.stringify(this.state))
    // console.log('sockets: ')

    let message = {
      user: socket.id,
      text: this.state.message
    }
    // console.log("onAddMessage: "+message.text)
    socket.emit('send:message', message)
    this.setState({message: ''})

    // console.log(JSON.stringify(this.state))
  }

  onChangeInput(e){
    this.setState({message: e.target.value})
  }

  discussionClick(index){
    this.setState({activeIndex: index})
  }

  render() {
    const { message, messages, discussions, activeIndex } = this.state;

    return (
      <div className="content">

        <DiscussionsList
          discussions={discussions}
          discussionClick={this.discussionClick.bind(this)}
          activeIndex={activeIndex}
        />


        <div className="main right">
          <div className="section">
            <div className="head">
              <div className="dot">...</div>
              <div className="title">
                Переписка с пользователем <a href="">{discussions[activeIndex]}</a>
              </div>
            </div>

            <MessageList messages={messages} />

            <MessageInputForm
              inputMessage={message}
              onChange={this.onChangeInput.bind(this)}
              onClick={this.onAddMessage.bind(this)}
              onKeyPress={this.handleOnKeyPress.bind(this)}
            />

          </div>
        </div>
        <div className="clear"></div>

      </div>
    )
  }
}

/*
* Discussions block (side left)
* */

let DiscussionsList = ({discussions, discussionClick, activeIndex}) => {
  return (
    <div className="sidebar left">
      <div className="section discussions_list">
        <div className="head">
          <div className="title">
            Все переписки
          </div>
        </div>
        <div className="body">
          <div className="discussions">

            {
              discussions.length > 0 ?
                discussions.map((item, index)=>{
                  return <Discussion item={item}
                                     index={index}
                                     discussionClick={discussionClick}
                                     isActive={activeIndex===index}
                                     key={index}
                  />
                })
                : null
            }

          </div>
        </div>


      </div>
    </div>
  )
}

class Discussion extends React.Component {

  handleClick() {
    this.props.discussionClick(this.props.index)
  }

  render () {
    // const {id} = this.props.item
    return (
      <a
        className={this.props.isActive ? 'discussion active' : 'discussion'}
        onClick={this.handleClick.bind(this)}
      >
        <div className="avatar"></div>
        <div className="info">
          <div className="username">{this.props.item}</div>
          <div className="login">[login]</div>
        </div>
      </a>
    )
  }
}


/*
* Messages Block (main)
* */

let MessageList = ({messages}) => {
  return (
    <div className="messages">
      {
        messages.length > 0 ?
          <Message messages={messages} />
          : <p style={{color: "grey"}}>сообщений нет</p>
      }
    </div>
  )
}

let Message = ({messages}) => {
  return (
    <div>
      {
        messages.map((item, i) => {
          return (
            <div className="message" key={i}>
              <div className="avatar"></div>
              <div className="info">
                <div className="date">[date]</div>
                <div className="username">{item.user}</div>
                <div className="text">{item.text}</div>
              </div>
            </div>
          )
        })
      }
    </div>
  )
}

let MessageInputForm = ({inputMessage, onChange, onClick, onKeyPress}) => {
  return (
  <div className="form_place">
    <div className="textarea">
      <textarea
        value={inputMessage}
        onChange={onChange}
        onKeyPress={onKeyPress}
      />
    </div>
    <button className="button btn-blue" onClick={onClick}>Написать</button>
  </div>
  )
}