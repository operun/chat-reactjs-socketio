import React from "react";
import './Footer.scss'

const Footer = () => {

  return (
    <footer>
      <div className="footer">
        <div className="inner">
          [footer]
        </div>
      </div>
    </footer>
  )
}

export default Footer