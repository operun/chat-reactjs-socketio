import React from "react"
import './Header.scss'

const Header = () => {

  return (
    <header>
      <div className="inner">
        <b>Мой круглый чат</b>
      </div>
    </header>
  )
}

export default Header