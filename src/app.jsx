import React from 'react'
import {render} from 'react-dom'
// import App from './chat'
import AppContainer from './containers/AppContainer'

render(
  <AppContainer />,
  document.getElementById('app-root')
)